﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace AspNet.Identity.MySQL
{
    public class ExecuteSqlCommand
    {
        private string query;
        public ExecuteSqlCommand(string Query)
        {
            query = Query;
        }

        public ExecuteSqlCommand()
        {
        }

        public int ExecuteInsertCommad()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            int result = cmd.ExecuteNonQuery();
            con.Close();
            return result;
        }

        public string ExecuteGetSingleValueCommand()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);

            SqlCommand cmd = new SqlCommand(query, con);
            con.Open();
            var result = cmd.ExecuteScalar();
            con.Close();
            if(!(result == null)){
                return result.ToString();
            }
            return "0";
        }
        
        //public DataTable ExecuteValueReader()
        //{
        //    string connectionString = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
        //    SqlConnection con = new SqlConnection(connectionString);

        //    SqlCommand cmd = new SqlCommand(query, con);
            
        //    con.Open();
        //    var read = cmd.ExecuteReader();
        //    DataTable Table = new DataTable();
        //    Table = null;
        //    Table = read.GetSchemaTable();
        //    string asd= Table.Columns.Count.ToString();
        //    con.Close();
        //    return Table;
        //}

        public SqlConnection getConnection()
        {
            string connectionString = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionString);

            return con;
        }

    }
}
