CREATE DATABASE CustomTest;

CREATE TABLE roles (
  Id varchar(50) NOT NULL,
  Name varchar(50) NOT NULL,
  PRIMARY KEY (Id)
);
spSearchByIdPurchaseOrder 1
CREATE PROCEDURE spSearchByIdPurchaseOrder
@id int
AS
BEGIN
	SELECT * FROM Purchase_Order
	WHERE ID = @id
END

CREATE PROCEDURE spSelectIdFromRolesUsingName
@Name varchar(50)
AS
BEGIN
	SELECT * FROM roles
	WHERE Name = @Name
END

SELECT * FROM branch

ALTER PROCEDURE spInsertIntoRoles
@id varchar(50),
@name varchar(50)
AS
BEGIN
	Insert into roles (Id, Name) values (@id, @name)
END

CREATE PROCEDURE spSelectNameFromRolesUsingId
@id varchar(50)
AS
BEGIN
	Select Name from roles where Id = @id
END

ALTER TABLE users
ADD CONSTRAINT unique_Username_OnUsers
UNIQUE(UserName)

CREATE TABLE users (
  Id varchar(50) NOT NULL,
  Email varchar(50) DEFAULT NULL,
  EmailConfirmed tinyint NOT NULL,
  PasswordHash varchar(200),
  SecurityStamp varchar(200),
  PhoneNumber varchar(200),
  PhoneNumberConfirmed tinyint NOT NULL,
  TwoFactorEnabled tinyint NOT NULL,
  LockoutEndDateUtc datetime DEFAULT NULL,
  LockoutEnabled tinyint NOT NULL,
  AccessFailedCount int NOT NULL,
  UserName varchar(50) NOT NULL,
  PRIMARY KEY (Id)
);

ALTER TABLE users
ADD Name VARCHAR(200)

CREATE TABLE userclaims (
  Id int NOT NULL IDENTITY,
  UserId varchar(50) NOT NULL,
  ClaimType varchar(200),
  ClaimValue varchar(200),
  PRIMARY KEY (Id),
  UNIQUE (Id),
  CONSTRAINT ApplicationUser_Claims FOREIGN KEY (UserId) REFERENCES users (Id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE userlogins (
  LoginProvider varchar(50) NOT NULL,
  ProviderKey varchar(50) NOT NULL,
  UserId varchar(50) NOT NULL,
  PRIMARY KEY (LoginProvider,ProviderKey,UserId),
  CONSTRAINT ApplicationUser_Logins FOREIGN KEY (UserId) REFERENCES users (Id) ON DELETE CASCADE ON UPDATE NO ACTION
);

CREATE TABLE userroles (
  UserId varchar(50) NOT NULL,
  RoleId varchar(50) NOT NULL,
  PRIMARY KEY (UserId,RoleId),
  CONSTRAINT ApplicationUser_Roles FOREIGN KEY (UserId) REFERENCES users (Id) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT IdentityRole_Users FOREIGN KEY (RoleId) REFERENCES roles (Id) ON DELETE CASCADE ON UPDATE NO ACTION
) ;

ALTER PROCEDURE spInsertIntoUsers
@Id varchar(50),
@Email varchar(50),
@EmailConfirmed tinyint,
@PasswordHash varchar(200),
@SecurityStamp varchar(200),
@PhoneNumber varchar(200),
@PhoneNumberConfirmed tinyint,
@TwoFactorEnabled tinyint,
@LockoutEndDateUtc datetime,
@LockoutEnabled tinyint,
@AccessFailedCount int,
@UserName varchar(50),
@Name varchar(200)
AS
BEGIN
	INSERT INTO users
	VALUES(@Id,@Email, @EmailConfirmed, @PasswordHash, @SecurityStamp, @PhoneNumber, @PhoneNumberConfirmed, @TwoFactorEnabled, @LockoutEndDateUtc, @LockoutEnabled, @AccessFailedCount, @UserName, @Name)
END



ALTER PROCEDURE spGetAllFromUsersByUserName
@UserName VARCHAR(50)
AS
BEGIN
	SELECT Id, Email, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumber, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEndDateUtc, LockoutEnabled, AccessFailedCount, UserName, Name
	FROM users
	WHERE users.UserName = @UserName
END

ALTER PROCEDURE spGetAllFromUsers
@UserName VARCHAR(50)
AS
BEGIN
	SELECT CAST(Id AS varchar(10)), Email, CAST(EmailConfirmed AS varchar(10)), PasswordHash, SecurityStamp, PhoneNumber, CAST(PhoneNumberConfirmed AS varchar(10)), CAST(TwoFactorEnabled AS varchar(10)), LockoutEndDateUtc, CAST(LockoutEnabled AS varchar(10)), CAST(AccessFailedCount AS varchar(10)), UserName, Name
	FROM users
	WHERE users.UserName = @UserName
END

ALTER PROCEDURE spGetAllFromUsersById
@Id varchar(50)
AS
BEGIN
	SELECT Id, Email, EmailConfirmed, PasswordHash, SecurityStamp, PhoneNumber, PhoneNumberConfirmed, TwoFactorEnabled, LockoutEndDateUtc, LockoutEnabled, AccessFailedCount, UserName, Name
	FROM users
	WHERE users.Id = @Id
END

ALTER PROCEDURE spGetPasswordHashFromUsersById
@Id varchar(50)
AS
BEGIN
	SELECT PasswordHash
	FROM users
	WHERE users.Id = @Id
END

CREATE PROCEDURE spInsertIntoUserLogins
@loginProvider varchar(50),
@providerKey varchar(50),
@userID varchar(50)
AS
BEGIN
	Insert into userlogins (LoginProvider, ProviderKey, UserId)
	values (@loginProvider, @providerKey, @userId)
END

CREATE PROCEDURE spGetUserIdByLoginFromUserLogins
@loginProvider varchar(50),
@providerKey varchar(50)
AS
BEGIN
	SELECT UserId FROM UserLogins
	WHERE (LoginProvider = @loginProvider) AND (ProviderKey = @providerKey)
END

CREATE PROCEDURE spGetByUserIdFromUserLogins
@userId varchar(50)
AS
BEGIN
	SELECT * FROM UserLogins 
	WHERE UserId = @userId
END

CREATE PROCEDURE spGetByUserIdFrom_Roles_and_UserRoles
@userId varchar(50)
AS
BEGIN
	SELECT Roles.Name FROM UserRoles, Roles 
	WHERE (UserRoles.UserId = @userId) AND (UserRoles.RoleId = Roles.Id)
END

CREATE PROCEDURE spInsertIntoUserRoles
@userId varchar(50),
@roleId varchar(50)
AS
BEGIN
	INSERT INTO UserRoles (UserId, RoleId)
	VALUES (@userId, @roleId)
END

CREATE PROCEDURE spGetSecurityStampFromUsers
@id varchar(50)
AS
BEGIN
	Select SecurityStamp from Users 
	where Id = @id
END

CREATE PROCEDURE spGetByUserIdFromUserClaims
@userId varchar(50)
AS
BEGIN
	Select * from UserClaims
	where UserId = @userId
END

CREATE PROCEDURE spInsertInToUserClaims
@value varchar(200),
@type varchar(200),
@userId varchar(50)
AS
BEGIN
	Insert into UserClaims (ClaimValue, ClaimType, UserId)
	values (@value, @type, @userId)
END

ALTER PROCEDURE spUpdateUser
@userId varchar(50),
@username varchar(50),
@pswHash varchar(200),
@secStamp varchar(200),
@email varchar(50),
@emailconfirmed tinyint,
@phonenumber varchar(200),
@phonenumberconfirmed tinyint,
@accesscount int,
@lockoutenabled tinyint,
@lockoutenddate datetime,
@twofactorenabled tinyint,
@Name varchar(200)
AS
BEGIN
Update Users set	UserName = @userName, PasswordHash = @pswHash, SecurityStamp = @secStamp, 
					Email = @email, EmailConfirmed = @emailconfirmed, PhoneNumber = @phonenumber, PhoneNumberConfirmed = @phonenumberconfirmed,
					AccessFailedCount = @accesscount, LockoutEnabled = @lockoutenabled, LockoutEndDateUtc = @lockoutenddate, TwoFactorEnabled = @twofactorenabled, Name = @Name
					WHERE Id = @userId
END

CREATE PROCEDURE spGetRoleNameUsingUserId
@UserName varchar(50)
AS
BEGIN
	DECLARE @UserId varchar(50)

	SELECT @UserId = Id FROM users
	WHERE @UserName = UserName

	SELECT Name FROM userroles JOIN roles ON userroles.RoleId = roles.Id
	WHERE userroles.UserId = @UserId
END

CREATE TABLE Company(
ComId int NOT NULL identity,
CompanyName varchar(200) NOT NULL,
RegistrationNo varchar(200) NOT NULL,
RegisteredCountry varchar(50) NOT NULL,
StreetAddress varchar(50) NOT NULL,
PhoneNo varchar(50) NOT NULL,
Email varchar(100) NOT NULL,
CONSTRAINT pk_Copanytbl
PRIMARY KEY(ComId),
CONSTRAINT unique_OnCompanytbl
UNIQUE(RegistrationNo)
);

ALTER TABLE CompanyUser
ADD CONSTRAINT unique_OnCompanyUser
UNIQUE(UserId)

CREATE TABLE CompanyUser(
UserId varchar(50) NOT NULL,
ComRegistrationNo varchar(200) NOT NULL,
CONSTRAINT pk_CompanyUsertbl
PRIMARY KEY(UserId,ComRegistrationNo),
CONSTRAINT fk_UserId_RefIdOnUser
FOREIGN KEY(UserId) REFERENCES users(Id),
CONSTRAINT fk_ComRegistrationNo_RefRegistrationNoOnCompany
FOREIGN KEY(ComRegistrationNo) REFERENCES Company(RegistrationNo)
);

ALTER PROCEDURE spInsertIntoCompanyUser
@UserId varchar(50),
@ComRegistrationNo varchar(200)
AS
BEGIN

	DECLARE @ComRegCheck varchar(50)
	SET @ComRegCheck = null

	SELECT @ComRegCheck = RegistrationNo
	FROM Company WHERE RegistrationNo = @ComRegistrationNo	
	
	IF @ComRegCheck = null
		BEGIN
			INSERT INTO Company
			VALUES('0',@ComRegistrationNo,'0','0','0','0') 
		END

	INSERT INTO CompanyUser
	VALUES (@UserId, @ComRegistrationNo)
END

CREATE PROCEDURE spGetUserIdFromCompanyUser
@UserId varchar(50)
AS
BEGIN
	SELECT ComRegistrationNo 
	FROM CompanyUser WHERE UserId = @UserId
END 

ALTER PROCEDURE spSearchEmployee
@UserId varchar(50),
@UserName varchar(50),
@ComRegNo varchar(50)
AS
BEGIN
	DECLARE @Uid varchar(50), @Uname varchar(50)

	SELECT * FROM users
	JOIN CompanyUser ON (users.Id = CompanyUser.UserId) AND (CompanyUser.ComRegistrationNo = @ComRegNo)
	WHERE (users.Id = @UserId) OR (users.UserName = @UserName)
END

CREATE PROCEDURE spDeleteFromUsers
@UserId varchar(50)
AS
BEGIN
	DELETE FROM users
	WHERE Id = @UserId
END


ALTER PROCEDURE spGetAllEmployees
@ComRegNo varchar(50)
AS
BEGIN
	SELECT users.Id, users.UserName, users.Email, users.PhoneNumber, users.AccessFailedCount, users.EmailConfirmed, users.LockoutEnabled, users.LockoutEndDateUtc, users.PasswordHash, users.PhoneNumberConfirmed, users.SecurityStamp, users.Name
	FROM users JOIN CompanyUser ON users.Id = CompanyUser.UserId
	WHERE CompanyUser.ComRegistrationNo = @ComRegNo
END

CREATE PROCEDURE spGetAllFromCompany
@ComRegNo varchar(50)
AS
BEGIN
	SELECT * FROM Company
	WHERE RegistrationNo = @ComRegNo
END

ALTER PROCEDURE spUpdateCompanyDetails
@ComId int,
@CompanyName varchar(200),
@RegistrationNo varchar(200),
@RegisteredCountry varchar(50),
@StreetAddress varchar(50),
@PhoneNo varchar(50),
@Email varchar(100)
AS
BEGIN
	UPDATE Company
	SET CompanyName = @CompanyName, RegistrationNo = @RegistrationNo , RegisteredCountry = @RegisteredCountry, StreetAddress = @StreetAddress, PhoneNo = @PhoneNo, Email = @Email
	WHERE ComId = @ComId
END

CREATE TABLE privilagestbl(
UserId varchar(50) NOT NULL,
RoleId varchar(50),
Function_1 tinyint,
Function_2 tinyint,
Function_3 tinyint,
PRIMARY KEY(UserId),
CONSTRAINT fk_UserId_RefIdOnUsers
FOREIGN KEY(UserId) REFERENCES users(Id),
CONSTRAINT fk_RoleId_RefIdOnRoles
FOREIGN KEY(RoleId) REFERENCES roles(Id)
);

ALTER PROCEDURE spGetPrivilages
@ComRegNo varchar(50),
@UserName varchar(50),
@UserId varchar(50)
AS
BEGIN
		DECLARE @Id varchar(50)

		SET @Id = @UserId

		IF(@Id IS NOT NULL)
		BEGIN
			SELECT privilagestbl.UserId, privilagestbl.RoleId, privilagestbl.Function_1, privilagestbl.Function_2, privilagestbl.Function_3
			FROM privilagestbl JOIN CompanyUser ON privilagestbl.UserId = CompanyUser.UserId
			WHERE CompanyUser.UserId = @UserId AND CompanyUser.ComRegistrationNo = @ComRegNo
			Return
		END
		ELSE
		BEGIN
			SELECT privilagestbl.UserId, privilagestbl.RoleId, privilagestbl.Function_1, privilagestbl.Function_2, privilagestbl.Function_3
			FROM (SELECT Id FROM users WHERE @UserName = UserName) as Temp JOIN CompanyUser ON CompanyUser.UserId = Temp.Id JOIN privilagestbl ON CompanyUser.ComRegistrationNo = @ComRegNo
		END	
END


ALTER PROCEDURE spInsertIntoPrivilages
@UserId varchar(50),
@Function_1 tinyint,
@Function_2 tinyint,
@Function_3 tinyint,
@ComRegNo varchar(50)
AS
BEGIN
	DECLARE @CheckUser varchar(50)

	SET @CheckUser = null;

	SELECT @CheckUser = Id FROM
	users JOIN CompanyUser ON users.Id = CompanyUser.UserId
	WHERE CompanyUser.ComRegistrationNo = @ComRegNo

	IF(@CheckUser = null)
	BEGIN
		RETURN
	END

	DECLARE @RoleId varchar(50)

	SELECT @RoleId = RoleId FROM userroles
	WHERE @UserId = UserId

	INSERT INTO privilagestbl
	VALUES (@UserId, @RoleId, @Function_1, @Function_2, @Function_2)
END



ALTER PROCEDURE spGetAllPrivilages
@ComRegNo varchar(50)
AS
BEGIN
	SELECT privilagestbl.UserId, privilagestbl.RoleId, privilagestbl.Function_1, privilagestbl.Function_2, privilagestbl.Function_3  FROM
	(SELECT * FROM CompanyUser WHERE @ComRegNo = ComRegistrationNo) as Temp JOIN privilagestbl
	ON privilagestbl.UserId = Temp.UserId 
END


ALTER PROCEDURE spGetUsersForAutoComplte
@UserName varchar(50),
@ComRegNo varchar(50)
AS
BEGIN
	DECLARE @SearchTerm VARCHAR(50)
	SET @SearchTerm = @UserName + '%'
	SELECT * FROM 
	users JOIN CompanyUser ON users.Id = CompanyUser.UserId
	WHERE users.UserName LIKE @SearchTerm
END

CREATE PROCEDURE spGetUsersForAutoComplteUsingd
@UserId varchar(50),
@ComRegNo varchar(50)
AS
BEGIN
	DECLARE @SearchTerm VARCHAR(50)
	SET @SearchTerm = @UserId + '%'
	SELECT * FROM 
	users JOIN CompanyUser ON users.Id = CompanyUser.UserId
	WHERE users.Id LIKE @SearchTerm
END

ALTER PROCEDURE spUpdatePrivilagesTbl
@UserId varchar(50),
@RoleId varchar(50),
@Function_1 tinyint,
@Function_2 tinyint,
@Function_3 tinyint
AS
BEGIN
	UPDATE privilagestbl SET privilagestbl.Function_1 = @Function_1, privilagestbl.Function_2 = @Function_2, privilagestbl.Function_3 = @Function_3 
	WHERE privilagestbl.UserId = @UserId

	UPDATE userroles SET RoleId = @RoleId
	WHERE UserId = @UserId
END

CREATE PROCEDURE spDeleteFromPrivlagetbl
@UserId varchar(50)
AS
BEGIN
	DELETE FROM privilagestbl WHERE UserId = @UserId	
END


CREATE TABLE Conversations(
MsgId INT IDENTITY,
UserId VARCHAR(50),
UserName VARCHAR(50),
ReceiverName VARCHAR(200),
UserGroup VARCHAR(50),
Msg NTEXT,
SentTime VARCHAR(50),
MsgDate VARCHAR(50),
CONSTRAINT pk_Conversations 
PRIMARY KEY(MsgId, UserId),
CONSTRAINT fk_UserId_OnConversations_refUserCompany
FOREIGN KEY(UserId) REFERENCES CompanyUser(UserId) ON UPDATE CASCADE,
CONSTRAINT fk_UserName_OnConversations_refUser
FOREIGN KEY(UserName) REFERENCES users(UserName) ON UPDATE CASCADE
);

ALTER PROCEDURE spInsertIntoCoversations
@UserId VARCHAR(50),
@UserName VARCHAR(50),
@ReceiverName VARCHAR(200),
@UserGroup VARCHAR(50),
@Msg NTEXT,
@SentTime VARCHAR(50),
@MsgDate VARCHAR(50)
AS
BEGIN
	INSERT INTO Conversations
	VALUES(@UserId, @UserName, @ReceiverName, @UserGroup, @Msg, @SentTime, @MsgDate)
END

ALTER PROCEDURE spSelectMassagesFromConversation
@UserId VARCHAR(50),
@UserName VARCHAR(50)
AS
BEGIN
	SELECT * FROM Conversations
	WHERE @UserId = UserId OR @UserName = ReceiverName
END

SELECT * FROM Notifications

ALTER PROCEDURE spSelectRangeFromNotifications
@Sdate DATE,
@Edate DATE,
@type VARCHAR(50)
AS
BEGIN
	SELECT * FROM Notifications
	WHERE (ReceiveTime > @Sdate and ReceiveTime < @Edate) and (NotificationType = @type or @type = '')
END

CREATE TABLE Notifications(
NotificationId INT IDENTITY,
SenderId VARCHAR(50),
ReceiverId VARCHAR(50),
NotificationType VARCHAR(50),
NotificationMsg NTEXT,
NotificationStatus VARCHAR(20),
ReceiveTime DATETIME,
ApprovedTime DATETIME,
CONSTRAINT pk_Notifications
PRIMARY KEY(NotificationId),
CONSTRAINT fk_SenderIdNotifications_RefUserIdCompanyUser
FOREIGN KEY(SenderId) REFERENCES CompanyUser(UserId),
CONSTRAINT fk_ReceiverIdNotifications_RefUserIdCompanyUser
FOREIGN KEY(ReceiverId) REFERENCES CompanyUser(UserId)
);

ALTER PROCEDURE spInsertIntoNotifications
@SenderUserName VARCHAR(50),
@ReceiverUserName VARCHAR(50),
@NotificationMsg NTEXT,
@ReceiveTime DATETIME,
@NotificationType VARCHAR(50),
@NotiID INT OUT
AS
BEGIN

	DECLARE @NotificationStatus varchar(20), @SenderId varchar(50), @ReceiverId varchar(50)

	SELECT @SenderId = Id FROM users WHERE @SenderUserName = UserName
	SELECT @ReceiverId = Id FROM users WHERE @ReceiverUserName = UserName

	SET @NotificationStatus = 'pending'

	INSERT INTO Notifications(SenderId, ReceiverId, NotificationMsg, ReceiveTime, NotificationType, NotificationStatus)
	VALUES(@SenderId, @ReceiverId, @NotificationMsg, @ReceiveTime, @NotificationType, @NotificationStatus)
	
	SELECT TOP 1 @NotiID = NotificationId
	FROM Notifications
	ORDER BY NotificationId DESC

END

ALTER PROCEDURE spSelectAllReceivedFromNotifications
@UserId VARCHAR(50)
AS
BEGIN
	SELECT * FROM Notifications
	WHERE ReceiverId = @UserId AND NotificationStatus = 'pending'
END

CREATE PROCEDURE spSelectAllSentFromNotifications
@UserId VARCHAR(50)
AS
BEGIN
	SELECT * FROM Notifications
	WHERE SenderId = @UserId
END


ALTER PROCEDURE spUpdateNotifications
@NotificationId INT,
@ApprovedTime DATETIME,
@NotificationStatus varchar(20)
AS
BEGIN
	UPDATE Notifications SET NotificationStatus = @NotificationStatus, ApprovedTime = @ApprovedTime 
	WHERE NotificationId = @NotificationId
END

CREATE PROCEDURE spGetUserNamrFromName
@Name VARCHAR(100)
AS
BEGIN
	SELECT UserName FROM users
	WHERE Name = @Name
END

CREATE PROCEDURE spGetUserIdFromUserName
@UserName VARCHAR(50)
AS
BEGIN
	SELECT Id FROM users
	WHERE UserName = @UserName
END

--usernames and passwords for team members
	--chanaka chanaka1991 idchanka
	--haswin haswin1990 idhaswin
	--shanika shanika1990 idshanika
	--himashi himashi1992 idhimashi

