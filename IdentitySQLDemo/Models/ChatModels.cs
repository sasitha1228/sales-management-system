﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IdentitySQLDemo.Models
{
    public class Message
    {
        public int MsgId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public string ReceiverName { get; set; }
        public string UserGroup { get; set; }
        public string Msg { get; set; }
        public string SentTime { get; set; }
        public string MsgDate { get; set; }
    }
}