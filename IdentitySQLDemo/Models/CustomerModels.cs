﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IdentitySQLDemo.Models
{
    public partial class customer
    {
        public int ID { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public int phone_no { get; set; }
        public string email { get; set; }
        public virtual ICollection<Purchase_Order> Purchase_Order { get; set; }
        public virtual ICollection<Purchase_Return_Order> Purchase_Return_Order { get; set; }
    }
    public class Purchase_Order
    {
        public string item_name { get; set; }
        public Nullable<int> ID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int num { get; set; }
        public string description { get; set; }
        public decimal unit_price { get; set; }
        public decimal quantity { get; set; }
        public decimal total_price { get; set; }
        public decimal Total { get { return (unit_price * quantity); } }
        public virtual customer customer { get; set; }
    }
    public class Purchase_Return_Order
    {
        public string item_name { get; set; }
        [HiddenInput(DisplayValue = false)]
        public Nullable<int> ID { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int num { get; set; }
        public string description { get; set; }
        public decimal unit_price { get; set; }
        public decimal quantity { get; set; }
        public decimal total_price { get; set; }
        public decimal Total { get { return (unit_price * quantity); } }
        public virtual customer customer { get; set; }
    }


}