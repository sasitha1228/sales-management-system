﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IdentitySQLDemo.Models
{

    public class SupplierAndDetails
    {
    }
    public class GRN
    {
        [Required(ErrorMessage = "Please Enter Correct GRN Number")]
        [Display(Name = "GRN Number")]
        public string GRN_NO { get; set; }
        [Required(ErrorMessage = "Please Enter Correct Supplier ID")]
        [Display(Name = "Supplier ID")]
        public int Supplier_id { get; set; }

        public string Descriptions { get; set; }
        public string Date { get; set; }

        [Display(Name = "Ordered Quantity")]
        public decimal Ordered_Quantity { get; set; }
        [Display(Name = "Delivered Quantity")]
        public decimal Delivered_Quantity { get; set; }
        [Display(Name = "Total Value")]
        public decimal Total_Value { get; set; }
        public decimal Discount { get; set; }
        [Display(Name = "Net Value")]
        public decimal Net_Value { get; set; }
        [Display(Name = "Net Value")]

        public decimal Net_Values { get { return (Total_Value - Discount); } }
        public virtual SupplierAndDetails SupplierAndDetails { get; set; }

    }
    public class Sales_Order
    {
        [Required(ErrorMessage = "Please Enter Correct Invoice Number")]
        [Display(Name = " Invoice Number")]

        public int Invoice_No { get; set; }
        [Required(ErrorMessage = "Please Enter Correct Supplier ID")]
        [Display(Name = " Supplier ID")]
        public int Supplier_id { get; set; }

        public string Descriptions { get; set; }
        public string Date { get; set; }
        public decimal Quntity { get; set; }
        [Display(Name = " Unit Price")]
        public decimal Unit_Price { get; set; }
        [Display(Name = " Total Price")]
        public decimal Total_Price { get; set; }
        public decimal Total { get { return (Unit_Price * Quntity); } }

        public virtual SupplierAndDetails SupplierAndDetails { get; set; }


    }

    //Connect with Sales_Return_Order table in Sales DB
    public class Sales_Return_Order
    {
        [Required(ErrorMessage = "Please Enter Correct Sales Return Number")]
        [Display(Name = "Sales Return Number")]
        public int SalesRe_No { get; set; }
        [Display(Name = "Invoice Number")]
        public int Invoice_No { get; set; }
        [Required(ErrorMessage = "Please Enter Correct Supplier ID")]
        [Display(Name = "Supplier ID")]
        public int Supplier_id { get; set; }

        public string Descriptions { get; set; }
        public string Date { get; set; }
        public decimal Quntity { get; set; }

        //[HiddenInput(DisplayValue = false)]
        [Display(Name = "Unit Price")]
        public decimal Unit_Price { get; set; }
        [Display(Name = "Total Price")]
        public decimal Total_Price { get; set; }
        public decimal Total { get { return (Unit_Price * Quntity); } }
        public virtual SupplierAndDetails SupplierAndDetails { get; set; }

    }
    public class Customer_Details
    {
        [Required(ErrorMessage = "Please Enter Correct Supplier ID")]
        [Display(Name = "Supplier ID")]
        public int Supplier_ID { get; set; }

        public string Name { get; set; }
        [Display(Name = "TelePhone")]
        public string Tphone { get; set; }
        public string Email { get; set; }
        public string Adress { get; set; }


        public virtual ICollection<Sales_Order> Sales_Order { get; set; }
        public virtual ICollection<GRN> GRN { get; set; }
        public virtual ICollection<Sales_Return_Order> Sales_Return_Order { get; set; }
    }
}