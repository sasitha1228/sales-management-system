﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IdentitySQLDemo.Models
{
    public class PRODUCTS
    {
        public int Product_Code { get; set; }
        public string Product_Name { get; set; }
        public DateTime Produced_Date { get; set; }
        public DateTime Expire_Date { get; set; }
        public decimal Unit_Price { get; set; }
        public decimal Quantity { get; set; }
        public string Manufacturer_Details { get; set; }



    }
    public class searchProductbyName
    {
        [Display(Name = "Product Name")]
        public string Name { get; set; }
    }
    public class EditDBData
    {
        [Display(Name = "Edit")]
        public string Name { get; set; }
        public string ID { get; set; }
    }
}
 

