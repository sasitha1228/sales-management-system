﻿using AspNet.Identity.MySQL;
using System.ComponentModel.DataAnnotations;
 
namespace IdentitySQLDemo.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
    }

    public class ApplicationRoles : IdentityRole
    {
    }
    public class ApplicationDbContext : SqlServer
    {
        public ApplicationDbContext(string connectionName)
            : base("UsersContext")
        {
        }
    }

    public class Company
    {
        [Display(Name = "Company ID")]
        public int ComId { get; set; }

        [Display(Name = "Company name")]
        public string CompanyName { get; set; }

        [Display(Name = "Company Registration No")]
        public string RegistrationNo { get; set; }

        [Display(Name = "Registered Country")]
        public string RegisteredCountry { get; set; }

        [Display(Name = "Street Address")]
        public string StreetAddress { get; set; }
        [DataType(DataType.PhoneNumber)]

        [Display(Name = "Contact No.")]
        public string PhoneNo { get; set; }
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }

    public class Privilage
    {
        [Display(Name = "User Id")]
        public string UserId { get; set; }

        [Display(Name = "Role Id")]
        public string RoleId { get; set; }

        [Display(Name = "Function 1")]
        public bool Function_1 { get; set; }
        
        [Display(Name = "Function 2")]
        public bool Function_2 { get; set; }
        
        [Display(Name = "Function 3")]
        public bool Function_3 { get; set; }
    }

    public class Notification
    {
        [Display(Name="Notification ID")]
        public int NotificationId { get; set; }
        
        [Display(Name = "Sender ID")]
        public string SenderId { get; set; }
        
        [Display(Name="User ID")]
        public string ReceiverId { get; set; }

        [Display(Name = "Notification")]
        public string NotificationMsg { get; set; }

        [Display(Name = "Status")]
        public string NotificationStatus { get; set; }

        [Display(Name = "Recevied Time")]
        public string ReceiveTime { get; set; }

        [Display(Name = "Approved Time")]
        public string ApprovedTime { get; set; }

        [Display(Name = "Authantication Type")]
        public string NotificationType { get; set; }
    }
}