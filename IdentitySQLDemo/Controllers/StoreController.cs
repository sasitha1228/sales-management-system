﻿using IdentitySQLDemo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IdentitySQLDemo.DB_layer;
using AspNet.Identity.MySQL;

namespace IdentitySQLDemo.Controllers
{
    public class StoreController : Controller
    {
        //
        // GET: /Store/
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        public ActionResult Product_Details()
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection stock = new SqlConnection(connectionstring);

            string name1 = "SELECT * FROM PRODUCTS";
            List<PRODUCTS> LIST = new List<PRODUCTS>();

            SqlCommand STOCK1 = new SqlCommand(name1, stock);
            stock.Open();
            var read = STOCK1.ExecuteReader();
            while (read.Read())
            {

                PRODUCTS product = new PRODUCTS();

                product.Product_Code = Convert.ToInt32(read["Product_Code"]);
                product.Product_Name = read["Product_Name"].ToString();
                product.Manufacturer_Details = read["Manufacturer_Details"].ToString();
                product.Produced_Date = Convert.ToDateTime(read["Produced_Date"]);
                product.Expire_Date = Convert.ToDateTime(read["Expire_Date"]);
                product.Unit_Price = decimal.Parse(read["Unit_Price"].ToString());
                product.Quantity = decimal.Parse(read["Quantity"].ToString());

                LIST.Add(product);




            }
            stock.Close();

            return View(LIST);

        }

        [Authorize]
        public ActionResult intsertdata()
        {

            return View();

        }

        [Authorize]
        [HttpPost]
        public ActionResult intsertdata(PRODUCTS variable)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection stock = new SqlConnection(connectionstring);
            string query = "Insert into PRODUCTS values (" + variable.Product_Code + ",'" + variable.Product_Name + "','" + variable.Manufacturer_Details + "','" + variable.Produced_Date + "','" + variable.Expire_Date + "'," + variable.Unit_Price + "," + variable.Quantity + ")";
            SqlCommand name2 = new SqlCommand(query, stock);
            stock.Open();
            name2.ExecuteNonQuery();
            stock.Close();


            return RedirectToAction("Product_Details");

        }

        [Authorize]
        [HttpGet]
        public ActionResult search()
        {

            return View();

        }

        [Authorize]
        [HttpPost]
        public ActionResult search(searchProductbyName searchResult)
        {

            string query = "select * from products where Product_Name = '" + searchResult.Name + "'";
            List<PRODUCTS> LIST = new List<PRODUCTS>();

            LIST = GetProduct(query);
            //PRODUCTS dfcd = LIST.ElementAtOrDefault(0);
            return View("search_Pst", LIST);

        }

        [Authorize]
        [HttpGet]
        public ActionResult EditDetails(int id)
        {
            string query = "SELECT * FROM PRODUCTS where Product_Code =" + id;
            List<PRODUCTS> LIST = new List<PRODUCTS>();
            LIST = GetProduct(query);

            return View(LIST.ElementAt(0));
        }

        [Authorize]
        [HttpPost]
        public ActionResult EditDetails(int id, PRODUCTS EditItem)
        {
            try
            {
                string query = "update products SET Product_name = '" + EditItem.Product_Name + "', Manufacturer_Details='" + EditItem.Manufacturer_Details + "', Produced_Date='" + EditItem.Produced_Date + "', Expire_Date='" + EditItem.Expire_Date + "', Unit_Price='" + EditItem.Unit_Price + "', Quantity='" + EditItem.Quantity + "' where Product_Code = '" + EditItem.Product_Code + "'";
                string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
                SqlConnection stock = new SqlConnection(connectionstring);

                SqlCommand name2 = new SqlCommand(query, stock);
                stock.Open();
                name2.ExecuteNonQuery();
                stock.Close();

                List<PRODUCTS> LIST = new List<PRODUCTS>();

                PRODUCTS EditElement = LIST.ElementAtOrDefault(0);
                LIST = GetProduct(query);
                return RedirectToAction("Product_Details", LIST);
            }
            catch
            {
                return View(EditItem);
            }

        }

        //[HttpGet]
        //public ActionResult DeleteDetails(int? id)
        //{

        //    return View();
        //}


        [Authorize]
        public ActionResult DeleteDetails(int id)
        {
            try
            {
                string query = "delete from PRODUCTS where Product_Code = '" + id + "'";
                string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
                SqlConnection stock = new SqlConnection(connectionstring);

                SqlCommand name2 = new SqlCommand(query, stock);
                stock.Open();
                name2.ExecuteNonQuery();
                stock.Close();
                List<PRODUCTS> LIST = new List<PRODUCTS>();
                PRODUCTS EditElement = LIST.ElementAtOrDefault(0);
                LIST = GetProduct(query);
                return RedirectToAction("Product_Details");
            }
            catch
            {
                return View();
            }

        }

        [Authorize]
        public List<PRODUCTS> GetProduct(string query)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection stock = new SqlConnection(connectionstring);
            List<PRODUCTS> LIST = new List<PRODUCTS>();

            SqlCommand STOCK1 = new SqlCommand(query, stock);
            stock.Open();
            var read = STOCK1.ExecuteReader();
            while (read.Read())
            {

                PRODUCTS product = new PRODUCTS();

                product.Product_Code = Convert.ToInt32(read["Product_Code"]);
                product.Product_Name = read["Product_Name"].ToString();
                product.Manufacturer_Details = read["Manufacturer_Details"].ToString();
                product.Produced_Date = Convert.ToDateTime(read["Produced_Date"]);
                product.Expire_Date = Convert.ToDateTime(read["Expire_Date"]);
                product.Unit_Price = decimal.Parse(read["Unit_Price"].ToString());
                product.Quantity = decimal.Parse(read["Quantity"].ToString());

                LIST.Add(product);

            }
            stock.Close();

            return LIST;
        }

        [Authorize]
        public ActionResult Products()
        {
            ViewBag.Message = "Your  page.";
            return View();
        }

        [Authorize]
        public ActionResult Stock()
        {
            ViewBag.Message = "Your  page.";
            return View();
        }

        [Authorize]
        public ActionResult Reports()
        {
            ViewBag.Message = "Your page.";
            return View();
        }

        [HttpGet]
        public ActionResult CheckAcceptabilyOfInvoices(string param, string type)
        {
            string query = "spCalculateGrn";
            
            if (String.Equals(type, "GRN"))
            {
                query = "spCalculateGrn";
            }
            else if (String.Equals(type, "Sales_Order"))
            {
                query = "spCalculateSalesOrder";
            }

            string result = (new BusinessLayer(query).GetResultsForInvoiceCheckWithDb(param, type)).ToString();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateNotification(string query)
        {
            new ExecuteSqlCommand(query).ExecuteInsertCommad();

            return Json("true", JsonRequestBehavior.AllowGet);
        }
	}
}