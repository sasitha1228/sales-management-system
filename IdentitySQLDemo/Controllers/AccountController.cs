﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using AspNet.Identity.MySQL;
using Microsoft.Owin.Security;
using IdentitySQLDemo.Models;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using IdentitySQLDemo.DB_layer;

namespace IdentitySQLDemo.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        public AccountController()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("UsersContext"))))
        {
        }

        public AccountController(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;

        }

        public AccountController(RoleManager<ApplicationRoles> roleManager)
        {
            RoleManager = roleManager;
        }

        public UserManager<ApplicationUser> UserManager { get; private set; }

        public RoleManager<ApplicationRoles> RoleManager { get; private set; }
        //
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        //
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {

            if (ModelState.IsValid)
            {
                var user = await UserManager.FindAsync(model.UserName, model.Password);
                if (user != null)
                {
                    await SignInAsync(user, model.RememberMe);
                    return RedirectToLocal(returnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Invalid username or password.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = new ApplicationUser() { UserName = model.UserName, Id = model.Id };
                var result = await UserManager.CreateAsync(user, model.Password);

                if (result.Succeeded)
                {
                    string Query = "spInsertIntoCompanyUser '" + model.Id + "','" + model.RegistrationNo + "'";
                    new ExecuteSqlCommand(Query).ExecuteInsertCommad();

                    await SignInAsync(user, isPersistent: false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    AddErrors(result);
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //Run Admin page
        [Authorize(Roles = "admin")]
        public ActionResult AdminPage()
        {
            return View();
        }

        //GET: register new employee
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult RegisterNew()
        {
            //Session["UserId"] = User.Identity.GetUserId();
            //Session["ComRegNo"] = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + Session["UserId"] + "'").ExecuteGetSingleValueCommand();

            return View();
        }


        //POST: Register new users action for admin users
        [HttpPost]
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> RegisterNew(RegisterViewModel model)
        {

            if (ModelState.IsValid)
            {
                model.RegistrationNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();
                var user = new ApplicationUser() { UserName = model.UserName, Id = model.Id, Name = model.Name };
                var result = await UserManager.CreateAsync(user, model.Password);

                string Query = "spInsertIntoCompanyUser '" + model.Id + "','" + model.RegistrationNo + "'";
                new ExecuteSqlCommand(Query).ExecuteInsertCommad();

                //string UserId = User.Identity.GetUserId();
                //string ComRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + UserId + "'").ExecuteGetSingleValueCommand();
            }
            return View();
        }

        //Auto complete user ID
        public JsonResult GetId(string term)
        {

            string query = "spGetUsersForAutoComplteUsingd '" + term + "','" + Session["ComRegNo"] + "'";
            List<IdentityUser> list = new List<IdentityUser>();
            BusinessLayer dbCon = new BusinessLayer(query);
            list = dbCon.GetUser();
            List<string> strList = list.Select(y => y.Id).ToList();

            return Json(strList, JsonRequestBehavior.AllowGet);
        }

        //Auto complete user name
        public JsonResult GetEmp(string term)
        {
            string comRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();
            string query = "spGetUsersForAutoComplte '" + term + "','" + comRegNo + "'";
            List<IdentityUser> list = new List<IdentityUser>();
            BusinessLayer dbCon = new BusinessLayer(query);
            list = dbCon.GetUser();

            List<string> strList = list.Select(y => y.UserName).ToList();

            return Json(strList, JsonRequestBehavior.AllowGet);
        }

        //
        // POST: /Account/Disassociate
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Disassociate(string loginProvider, string providerKey)
        {
            ManageMessageId? message = null;
            IdentityResult result = await UserManager.RemoveLoginAsync(User.Identity.GetUserId(), new UserLoginInfo(loginProvider, providerKey));
            if (result.Succeeded)
            {
                message = ManageMessageId.RemoveLoginSuccess;
            }
            else
            {
                message = ManageMessageId.Error;
            }
            return RedirectToAction("Manage", new { Message = message });
        }

        //
        // GET: /Account/Manage
        public ActionResult Manage(ManageMessageId? message)
        {
            ViewBag.StatusMessage =
                message == ManageMessageId.ChangePasswordSuccess ? "Your password has been changed."
                : message == ManageMessageId.SetPasswordSuccess ? "Your password has been set."
                : message == ManageMessageId.RemoveLoginSuccess ? "The external login was removed."
                : message == ManageMessageId.Error ? "An error has occurred."
                : "";
            ViewBag.HasLocalPassword = HasPassword();
            ViewBag.ReturnUrl = Url.Action("Manage");
            return View();
        }

        //
        // POST: /Account/Manage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Manage(ManageUserViewModel model)
        {
            bool hasPassword = HasPassword();
            ViewBag.HasLocalPassword = hasPassword;
            ViewBag.ReturnUrl = Url.Action("Manage");
            if (hasPassword)
            {
                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.ChangePasswordAsync(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.ChangePasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }
            else
            {
                // User does not have a password so remove any validation errors caused by a missing OldPassword field
                ModelState state = ModelState["OldPassword"];
                if (state != null)
                {
                    state.Errors.Clear();
                }

                if (ModelState.IsValid)
                {
                    IdentityResult result = await UserManager.AddPasswordAsync(User.Identity.GetUserId(), model.NewPassword);
                    if (result.Succeeded)
                    {
                        return RedirectToAction("Manage", new { Message = ManageMessageId.SetPasswordSuccess });
                    }
                    else
                    {
                        AddErrors(result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        //Add users to role action for admin users
        [HttpGet]
        [Authorize(Roles = "admin")]
        public ActionResult AddUserToRole()
        {
            return View();
        }

        //Add users to role action for admin users
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddUserToRole(InsertUsersToRolesModel model)
        {
            if (ModelState.IsValid)
            {
                var result = await UserManager.AddToRoleAsync(model.Id, model.RoleName);
            }

            return View(model);
        }

        public ActionResult SelectNotification()
        {
            return View();
        }

        public ActionResult NotifiSelect(DateTime sdate, DateTime edate, string type)
        {
            string query = "spSelectRangeFromNotifications '" + sdate + "','" + edate + "','" + type + "'";

            BusinessLayer blsyer = new BusinessLayer(query);

            List<Notification> list = blsyer.GetNotification();

            return View(list);
        }

        [Authorize]
        public ActionResult Notifications()
        {
            string query = "spSelectAllReceivedFromNotifications '" + User.Identity.GetUserId() + "'";

            List<Notification> list = new List<Notification>();
            BusinessLayer getList = new BusinessLayer(query);

            list = getList.GetNotification();

            return View(list);
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var user = await UserManager.FindAsync(loginInfo.Login);
            if (user != null)
            {
                await SignInAsync(user, isPersistent: false);
                return RedirectToLocal(returnUrl);
            }
            else
            {
                // If the user does not have an account, then prompt the user to create an account
                ViewBag.ReturnUrl = returnUrl;
                ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { UserName = loginInfo.DefaultUserName });
            }
        }

        //
        // POST: /Account/LinkLogin
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LinkLogin(string provider)
        {
            // Request a redirect to the external login provider to link a login for the current user
            return new ChallengeResult(provider, Url.Action("LinkLoginCallback", "Account"), User.Identity.GetUserId());
        }

        //
        // GET: /Account/LinkLoginCallback
        public async Task<ActionResult> LinkLoginCallback()
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync(XsrfKey, User.Identity.GetUserId());
            if (loginInfo == null)
            {
                return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
            }
            var result = await UserManager.AddLoginAsync(User.Identity.GetUserId(), loginInfo.Login);
            if (result.Succeeded)
            {
                return RedirectToAction("Manage");
            }
            return RedirectToAction("Manage", new { Message = ManageMessageId.Error });
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new ApplicationUser() { UserName = model.UserName };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInAsync(user, isPersistent: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        [ChildActionOnly]
        public ActionResult RemoveAccountList()
        {
            var linkedAccounts = UserManager.GetLogins(User.Identity.GetUserId());
            ViewBag.ShowRemoveButton = HasPassword() || linkedAccounts.Count > 1;
            return (ActionResult)PartialView("_RemoveAccountPartial", linkedAccounts);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing && UserManager != null)
            {
                UserManager.Dispose();
                UserManager = null;
            }
            base.Dispose(disposing);
        }

        //GET: Find user action for admin users
        public ActionResult FindUser()
        {
            return View();
        }

        //POST: Find user action for admin users
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FindUser_Post(FindEmployee model)
        {

            SqlConnection con = new ExecuteSqlCommand().getConnection();

            List<IdentityUser> list = new List<IdentityUser>();

            string query = "";

            if (ModelState.IsValid)
            {
                string comRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();

                query = "spSearchEmployee '" + model.Id + "','" + model.UserName + "','" + comRegNo + "'";

            }
            BusinessLayer dbCon = new BusinessLayer(query);

            list = dbCon.GetUser();
            return View(list);
        }

        //Get my details action for any employee
        [Authorize]
        public async Task<ActionResult> GetMyDetails()
        {
            var user = await UserManager.FindByNameAsync(User.Identity.Name);

            return View(user);
        }

        //GET: Edit details acction for any user
        [Authorize]
        public ActionResult EditMyDetails(ApplicationUser user)
        {
            return View(user);
        }

        //POST: Edit details acction for any user
        [HttpPost]
        [ActionName("EditMyDetails")]
        public async Task<ActionResult> EditMyDetails_Post(ApplicationUser user)
        {
            var result = await UserManager.UpdateAsync(user);

            if (result.Succeeded)
            {
                return RedirectToAction("GetMyDetails");
            }

            return View();
        }

        //Get All Employee action for admin users
        [Authorize]
        public ActionResult GetAllEmployees()
        {
            List<IdentityUser> list = new List<IdentityUser>();
            string query = "spGetAllEmployees '" + Session["ComRegNo"] + "'";

            BusinessLayer dbCon = new BusinessLayer(query);
            list = dbCon.GetUser();

            return View(list);
        }

        //Delete Employee action for admin users
        [Authorize(Roles = "admin")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteEmployee(ApplicationUser user)
        {
            await UserManager.DeleteAsync(user);
            return RedirectToAction("FindUser");
        }

        //Get all user privilages
        public ActionResult GetAllPrivilages()
        {
            string comRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();

            var list = GetPrivilageDb("spGetAllPrivilages '" + comRegNo + "'");

            return View(list);
        }

        //GET: get privilages of a specific user
        [Authorize(Roles = "admin")]
        public ActionResult GetPrivilages()
        {
            return View();
        }

        //POST: get privilages of a specific user
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetPrivilages_Post(FindEmployee model)
        {
            List<Privilage> list = new List<Privilage>();
            if (ModelState.IsValid)
            {
                string comRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();

                string Query = "spGetPrivilages '" + comRegNo + "','" + model.UserName + "','" + model.Id + "'";
                list = GetPrivilageDb(Query);
            }
            return View(list);
        }

        //GET: Edit Privilages
        public ActionResult EditPrivilages(Privilage privilage)
        {
            return View(privilage);
        }

        //POST: Edit Privilages
        [HttpPost]
        public ActionResult EditPrivilages_Post(Privilage privilage)
        {
            if (ModelState.IsValid)
            {
                byte function_1 = 0;
                byte function_2 = 0;
                byte function_3 = 0;

                if (privilage.Function_1)
                {
                    function_1 = 1;
                }

                if (privilage.Function_2)
                {
                    function_2 = 1;
                }

                if (privilage.Function_3)
                {
                    function_3 = 1;
                }

                string query = "spUpdatePrivilagesTbl '" + privilage.UserId + "','" + privilage.RoleId + "'," + function_1 + "," + function_2 + "," + function_3;
                new ExecuteSqlCommand(query).ExecuteInsertCommad();
            }
            return RedirectToAction("GetAllPrivilages");
        }

        //Delete privilages
        public ActionResult DeletePrivilage(string userId)
        {
            string query = "spDeleteFromPrivlagetbl '" + userId + "'";
            new ExecuteSqlCommand(query).ExecuteInsertCommad();

            return RedirectToAction("GetAllPrivilages");
        }

        public List<Privilage> GetPrivilageDb(string query)
        {


            List<Privilage> list = new List<Privilage>();
            BusinessLayer dbCon = new BusinessLayer(query);

            list = dbCon.GetPrivilageDb();

            return list;
        }


        //GET: add privilages
        [Authorize(Roles = "admin")]
        public ActionResult Addprivilages()
        {
            return View();
        }

        //POST: add privilages
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Addprivilages_Post(Privilage privilages)
        {
            if (ModelState.IsValid)
            {
                string Function_1 = "0";
                string Function_2 = "0";
                string Function_3 = "0";

                if (privilages.Function_1)
                {
                    Function_1 = "1";
                }

                if (privilages.Function_2)
                {
                    Function_2 = "1";
                }

                if (privilages.Function_3)
                {
                    Function_3 = "1";
                }

                string query = "spInsertIntoPrivilages '" + privilages.UserId + "'," + Function_1 + "," + Function_2 + "," + Function_3 + ",'" + Session["ComRegNo"] + "'";

                new ExecuteSqlCommand(query).ExecuteInsertCommad();
            }
            return RedirectToAction("GetAllPrivilages");
        }

        //Get my privilages
        public ActionResult MyPrvilages()
        {
            string query = "spGetPrivilages '" + Session["ComRegNo"] + "','','" + User.Identity.GetUserId() + "'";
            var list = GetPrivilageDb(query);
            return View(list);
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent)
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ExternalCookie);
            var identity = await UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            AuthenticationManager.SignIn(new AuthenticationProperties() { IsPersistent = isPersistent }, identity);

            Session["UserId"] = User.Identity.GetUserId();
            Session["ComRegNo"] = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + Session["UserId"] + "'").ExecuteGetSingleValueCommand();
        }

        //Get Company details action for admin users

        public ActionResult CompanyDetails()
        {
            SqlConnection con = new ExecuteSqlCommand().getConnection();

            var comRegNo = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + User.Identity.GetUserId() + "'").ExecuteGetSingleValueCommand();

            string query = "spGetAllFromCompany '" + comRegNo + "'";

            Company company = new Company();
            BusinessLayer dbCon = new BusinessLayer(query);

            company = dbCon.GetCompanyDetails();

            return View(company);
        }


        //Edit company details action for admin users
        [Authorize(Roles = "admin")]
        public ActionResult EditCompanyDetails_Get(Company company)
        {
            return View(company);
        }

        //Edit company details action for admin users
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "admin")]
        public ActionResult EditCompanyDetails(Company company)
        {
            if (ModelState.IsValid)
            {
                string query = "spUpdateCompanyDetails " + company.ComId + ",'" + company.CompanyName + "','" + company.RegistrationNo + "','" + company.RegisteredCountry + "','" + company.StreetAddress + "','" + company.PhoneNo + "','" + company.Email + "'";

                new ExecuteSqlCommand(query).ExecuteInsertCommad();
            }
            return RedirectToAction("CompanyDetails");
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

       

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private bool HasPassword()
        {
            var user = UserManager.FindById(User.Identity.GetUserId());
            if (user != null)
            {
                return user.PasswordHash != null;
            }
            return false;
        }

        public enum ManageMessageId
        {
            ChangePasswordSuccess,
            SetPasswordSuccess,
            RemoveLoginSuccess,
            Error
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Home");
            }
        }

        private class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties() { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}