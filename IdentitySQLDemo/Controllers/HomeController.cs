﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using AspNet.Identity.MySQL;
using IdentitySQLDemo.Models;
using System.Data.SqlClient;

namespace IdentitySQLDemo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["UserId"] = User.Identity.GetUserId();
            Session["UserName"] = User.Identity.GetUserName();
            Session["ComRegNo"] = new ExecuteSqlCommand("spGetUserIdFromCompanyUser '" + Session["UserId"] + "'").ExecuteGetSingleValueCommand();

            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
        public ActionResult Chat()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public PartialViewResult getAllMessages()
        {
            string query = "spSelectMassagesFromConversation '" + User.Identity.GetUserId() + "','" + User.Identity.GetUserName() + "'";
            List<Message> AllMessages = getMsg(query);
            return PartialView("_Messages", AllMessages);
        }

        public PartialViewResult getLastTenMessages()
        {
            string query = "spSelectMassagesFromConversation '" + User.Identity.GetUserId() + "','" + User.Identity.GetUserName() + "'";
            List<Message> lastTenMessages = getMsg(query).OrderByDescending(x => x.MsgId).Take(10).ToList();
            return PartialView("_Messages", lastTenMessages);
        }

        public ActionResult nortifications()
        {

            return View();
        }

        public PartialViewResult ParNortifications(string msg)
        {
            ViewBag.Msg = msg;
            return PartialView("_notifications");
        }
        public List<Message> getMsg(string Query)
        {
            List<Message> msgList = new List<Message>();
            SqlConnection con = new ExecuteSqlCommand().getConnection();
            SqlCommand cmd = new SqlCommand(Query, con);

            con.Open();
            var read = cmd.ExecuteReader();
            while (read.Read())
            {
                Message msg = new Message();

                msg.MsgId = int.Parse(read["MsgId"].ToString());
                msg.UserId = read["UserId"].ToString();
                msg.UserName = read["UserName"].ToString();
                msg.ReceiverName = read["ReceiverName"].ToString();
                msg.Msg = read["Msg"].ToString();
                msg.UserGroup = read["UserGroup"].ToString();
                msg.SentTime = read["SentTime"].ToString();
                msg.MsgDate = read["MsgDate"].ToString();

                msgList.Add(msg);
                
            }
            con.Close();
            return msgList;
        }
    }
}