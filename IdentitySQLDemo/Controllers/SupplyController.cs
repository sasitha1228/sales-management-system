﻿using IdentitySQLDemo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using AspNet.Identity.MySQL;
using IdentitySQLDemo.DB_layer;

namespace IdentitySQLDemo.Controllers
{
    public class SupplyController : Controller
    {
        //
        // GET: /Supply/
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }

        [Authorize]
        [HttpGet]
        public ActionResult GRN_Inset()
        {
            return View();
        }
        // insert to GRN

        [Authorize]
        [HttpPost]
        public ActionResult GRN_Inset(GRN variable1)
        {
            string query2 = "Insert into GRN (GRN_NO,Supplier_id,Descriptions,Date,Ordered_Quantity,Delivered_Quantity,Total_Value,Discount) values ('" + variable1.GRN_NO + "'," + variable1.Supplier_id + ",'" + variable1.Descriptions + "', '" + variable1.Date + "', " + variable1.Ordered_Quantity + " ," + variable1.Delivered_Quantity + "," + variable1.Total_Value + "," + variable1.Discount + ")";
           new ExecuteSqlCommand(query2).ExecuteInsertCommad();

            return View();
        }

        //insert multiple grn
        [Authorize]
        public ActionResult GRN_Insetmulti()
        {
            List<GRN> grn = new List<GRN> { new GRN { GRN_NO = "", Supplier_id = 0, Descriptions = "", Date = "", Ordered_Quantity = 0, Delivered_Quantity = 0, Total_Value = 0, Discount = 0 } };
            return View(grn);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GRN_Insetmulti(List<GRN> grn)
        {
            if (ModelState.IsValid)
            {

                foreach (var grns in grn)
                {
                    string query2 = "Insert into GRN(GRN_NO,Supplier_id,Descriptions,Date,Ordered_Quantity,Delivered_Quantity,Total_Value,Discount values (" + grns.GRN_NO + "," + grns.Supplier_id + ",'" + grns.Descriptions + "', '" + grns.Date + "', " + grns.Ordered_Quantity + "," + grns.Delivered_Quantity + " ," + grns.Total_Value + "," + grns.Discount + " )";
                    new ExecuteSqlCommand(query2).ExecuteInsertCommad();

                }
            }
            return View(grn);

        }


        //view GRN
        [Authorize]
        public ActionResult GRN_view()
        {

            string name2 = "SELECT GRN_NO,Supplier_id,Descriptions,Date,Ordered_Quantity,Delivered_Quantity,Total_Value,Discount FROM GRN  ";
            List<GRN> LIST1 = new List<GRN>();
            LIST1 = new BusinessLayer(name2).GetGrn();

            return View(LIST1);

        }

        [Authorize]
        [HttpGet]
        public ActionResult GRN_Edit(int grno)
        {

            SqlConnection Sales = new ExecuteSqlCommand().getConnection();

            string name2 = "SELECT GRN_NO,Supplier_id,Descriptions,Date,Ordered_Quantity,Delivered_Quantity,Total_Value,Discount FROM GRN where GRN_NO=" + grno;
            List<GRN> LIST1 = new List<GRN>();
            LIST1 = new BusinessLayer(name2).GetGrn();

            return View(LIST1.ElementAt(0));

        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GRN_Edit(GRN variable2)
        {
            string query4 = "Update GRN set GRN_NO=" + variable2.GRN_NO + ",Supplier_id=" + variable2.Supplier_id + ",Descriptions='" + variable2.Descriptions + "', Date='" + variable2.Date + "', Ordered_Quantity =" + variable2.Ordered_Quantity + ",Delivered_Quantity=" + variable2.Delivered_Quantity + " ,Total_Value=" + variable2.Total_Value + ",Discount=" + variable2.Discount + "  ";
            new ExecuteSqlCommand(query4).ExecuteInsertCommad();
            ViewBag.Message = "Data succesfully saved";

            return RedirectToAction("GRN_view");
        }


        //Delete sales order 
        [Authorize]
        [HttpGet]
        public ActionResult GRN_Delete(int grno)
        {

            SqlConnection Sales2 = new ExecuteSqlCommand().getConnection();

            string name2 = "SELECT GRN_NO,Supplier_id,Descriptions,Date,Ordered_Quantity,Delivered_Quantity,Total_Value,Discount FROM GRN WHERE GRN_NO=" + grno;
            List<GRN> LIST1 = new List<GRN>();

            SqlCommand Sales3 = new SqlCommand(name2, Sales2);
            Sales2.Open();
            var read = Sales3.ExecuteReader();
            GRN grn = new GRN();
            while (read.Read())
            {



                grn.GRN_NO = read["GRN_NO"].ToString();
                grn.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                grn.Descriptions = read["Descriptions"].ToString();
                grn.Date = read["Date"].ToString();
                grn.Ordered_Quantity = Convert.ToInt32(read["Ordered_Quantity"]);
                grn.Delivered_Quantity = Convert.ToInt32(read["Delivered_Quantity"]);
                grn.Total_Value = Convert.ToInt32(read["Total_Value"]);
                grn.Discount = Convert.ToInt32(read["Discount"]);
                //grn.Net_Value = Convert.ToInt32(read["Net_Value"]);






            }
            Sales2.Close();

            return View(grn);

        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GRN_Delete(GRN variable3)
        {

            SqlConnection Sales6 = new ExecuteSqlCommand().getConnection();
            string deleteQuery = "DELETE  FROM GRN WHERE GRN_NO = " + variable3.GRN_NO;
            SqlCommand name5 = new SqlCommand(deleteQuery, Sales6);
            Sales6.Open();
            name5.ExecuteNonQuery();
            Sales6.Close();


            return RedirectToAction("GRN_view");

        }

        //SALES RETURN METHODS
        [Authorize]
        [HttpGet]
        public ActionResult Sales_Return_Order_Inset()
        {
            return View();
        }
        // insert to Sales return order
        [Authorize]
        [HttpPost]
        public ActionResult Sales_Return_Order_Inset(Sales_Return_Order variable2)
        {

            SqlConnection Sales1 = new ExecuteSqlCommand().getConnection();
            string query1 = "Insert into Sales_Return_Order(SalesRe_No,Invoice_No,Supplier_id,Descriptions,Date,Quntity ,Unit_Price ) values (" + variable2.SalesRe_No + "," + variable2.Invoice_No + "," + variable2.Supplier_id + ",'" + variable2.Descriptions + "', '" + variable2.Date + "', " + variable2.Quntity + " ," + variable2.Unit_Price + ")";
            SqlCommand name5 = new SqlCommand(query1, Sales1);
            Sales1.Open();
            name5.ExecuteNonQuery();
            Sales1.Close();


            return RedirectToAction("Sales_Return_Order_view");

        }


        //insert multiple data into the sales return order

        [Authorize]
        public ActionResult Sales_Return_Order_Insetmulti()
        {
            List<Sales_Return_Order> sro = new List<Sales_Return_Order> { new Sales_Return_Order { SalesRe_No = 0, Invoice_No = 0, Supplier_id = 0, Descriptions = "", Date = "", Quntity = 0, Unit_Price = 0 } };
            return View(sro);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sales_Return_Order_Insetmulti(List<Sales_Return_Order> sro)
        {
            if (ModelState.IsValid)
            {

                SqlConnection Sales3 = new ExecuteSqlCommand().getConnection();

                Sales3.Open();
                foreach (var sros in sro)
                {
                    string query2 = "Insert into Sales_Return_Order (SalesRe_No,Invoice_No,Supplier_id,Descriptions,Date,Quntity,Unit_Price) values (" + sros.SalesRe_No + "," + sros.Invoice_No + "," + sros.Supplier_id + ",'" + sros.Descriptions + "', '" + sros.Date + "', " + sros.Quntity + "," + sros.Unit_Price + "  )";
                    SqlCommand name4 = new SqlCommand(query2, Sales3);

                    name4.ExecuteNonQuery();

                }
                Sales3.Close();

            }
            return View(sro);

        }

        //view sales return order
        [Authorize]
        public ActionResult Sales_Return_Order_view()
        {


            SqlConnection Sales2 = new ExecuteSqlCommand().getConnection();

            string name6 = "SELECT SalesRe_No,Invoice_No,Supplier_id,Descriptions,Date,Quntity,Unit_Price FROM   Sales_Return_Order  ";
            List<Sales_Return_Order> LIST1 = new List<Sales_Return_Order>();

            SqlCommand Sales3 = new SqlCommand(name6, Sales2);
            Sales2.Open();
            var read = Sales3.ExecuteReader();
            while (read.Read())
            {

                Sales_Return_Order SRO = new Sales_Return_Order();

                SRO.SalesRe_No = Convert.ToInt32(read["SalesRe_No"]);
                SRO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SRO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SRO.Descriptions = read["Descriptions"].ToString();
                SRO.Date = read["Date"].ToString();
                SRO.Quntity = Convert.ToInt32(read["Quntity"]);
                SRO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);
                LIST1.Add(SRO);




            }
            Sales2.Close();

            return View(LIST1);


        }



        //Edit sales return order

        [Authorize]
        [HttpGet]
        public ActionResult Sales_Return_Order_Edit(int srn)
        {


            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();

            string name3 = "SELECT  SalesRe_No, Invoice_No ,Supplier_id , Descriptions,Date,Quntity,Unit_Price FROM Sales_Return_Order   WHERE SalesRe_No =" + srn;
            List<Sales_Return_Order> LIST2 = new List<Sales_Return_Order>();

            SqlCommand Sales2 = new SqlCommand(name3, Sales4);
            Sales4.Open();
            var read = Sales2.ExecuteReader();
            Sales_Return_Order SRO = new Sales_Return_Order();
            while (read.Read())
            {


                SRO.SalesRe_No = Convert.ToInt32(read["SalesRe_No"]);
                SRO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SRO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SRO.Descriptions = read["Descriptions"].ToString();
                SRO.Date = read["Date"].ToString();
                SRO.Quntity = Convert.ToInt32(read["Quntity"]);
                SRO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);







            }
            Sales4.Close();

            return View(SRO);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sales_Return_Order_Edit(Sales_Return_Order variable2)
        {

            SqlConnection Sales5 = new ExecuteSqlCommand().getConnection();
            string query4 = "Update Sales_Return_Order set SalesRe_No=" + variable2.SalesRe_No + ", Invoice_No=" + variable2.Invoice_No + ",Supplier_id=" + variable2.Supplier_id + ",Descriptions='" + variable2.Descriptions + "', Date='" + variable2.Date + "', Quntity =" + variable2.Quntity + ",Unit_Price=" + variable2.Unit_Price + " ";
            SqlCommand name4 = new SqlCommand(query4, Sales5);
            Sales5.Open();
            name4.ExecuteNonQuery();
            ViewBag.Message = "Data succesfully saved";

            Sales5.Close();

            return RedirectToAction("Sales_Return_Order_view");

        }


        //Delete sales return order 

        [Authorize]
        [HttpGet]
        public ActionResult Sales_Return_Order_Delete(int srn)
        {

            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();

            string name3 = "SELECT SalesRe_No, Invoice_No ,Supplier_id , Descriptions,Date,Quntity,Unit_Price FROM  Sales_Return_Order  WHERE SalesRe_No=" + srn;
            List<Sales_Return_Order> LIST3 = new List<Sales_Return_Order>();

            SqlCommand Sales2 = new SqlCommand(name3, Sales4);
            Sales4.Open();
            var read = Sales2.ExecuteReader();
            Sales_Return_Order SRO = new Sales_Return_Order();
            while (read.Read())
            {


                SRO.SalesRe_No = Convert.ToInt32(read["SalesRe_No"]);
                SRO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SRO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SRO.Descriptions = read["Descriptions"].ToString();
                SRO.Date = read["Date"].ToString();
                SRO.Quntity = Convert.ToInt32(read["Quntity"]);
                SRO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);





            }
            Sales4.Close();

            return View(SRO);


        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sales_Return_Order_Delete(Sales_Return_Order variable3)
        {

            SqlConnection Sales5 = new ExecuteSqlCommand().getConnection();
            string deleteQuery = "DELETE  FROM Sales_Return_Order WHERE SalesRe_No = " + variable3.SalesRe_No;
            SqlCommand name5 = new SqlCommand(deleteQuery, Sales5);
            Sales5.Open();
            name5.ExecuteNonQuery();
            Sales5.Close();


            return RedirectToAction("Sales_Return_Order_view");

        }

        //View supplier details
        [Authorize]
        public ActionResult Supplier_Detail_view(int? page)
        {
            {

                SqlConnection Sales = new ExecuteSqlCommand().getConnection();

                string name2 = "SELECT  Supplier_ID,Name,Tphone,Email,Adress  FROM   Customer_Details ";
                List<Customer_Details> LIST1 = new List<Customer_Details>();

                SqlCommand Sales1 = new SqlCommand(name2, Sales);
                Sales.Open();
                var read = Sales1.ExecuteReader();
                while (read.Read())
                {

                    Customer_Details CD = new Customer_Details();

                    CD.Supplier_ID = Convert.ToInt32(read["Supplier_ID"]);
                    CD.Name = read["Name"].ToString();
                    CD.Tphone = read["Tphone"].ToString();
                    CD.Email = read["Email"].ToString();
                    CD.Adress = read["Adress"].ToString();
                    LIST1.Add(CD);


                }
                Sales.Close();

                return View(LIST1.ToPagedList(page ?? 1, 6));

            }

        }

        //search supplier details
        [Authorize]
        public ActionResult search_Supplier_Detail(string search, int? page)
        {


            SqlConnection Sales = new ExecuteSqlCommand().getConnection();

            string searchquery = " sp_customer_Details1  '" + search + "' ";
            List<Customer_Details> LIST1 = new List<Customer_Details>();


            SqlCommand Sales2 = new SqlCommand(searchquery, Sales);
            Sales.Open();
            var read = Sales2.ExecuteReader();
            while (read.Read())
            {

                Customer_Details CD = new Customer_Details();

                CD.Supplier_ID = Convert.ToInt32(read["Supplier_ID"]);
                CD.Name = read["Name"].ToString();
                CD.Tphone = read["Tphone"].ToString();
                CD.Email = read["Email"].ToString();
                CD.Adress = read["Adress"].ToString();
                LIST1.Add(CD);


            }
            Sales.Close();

            return View(LIST1.ToPagedList(page ?? 1, 6));


        }


        //Insert supplier details
        [Authorize]
        [HttpGet]
        public ActionResult Supplier_Detail_Inset()
        {
            return View();
        }


        [Authorize]
        [HttpPost]
        public ActionResult Supplier_Detail_Inset(Customer_Details variable)
        {

            SqlConnection Sales2 = new ExecuteSqlCommand().getConnection();
            string query1 = "Insert into Customer_Details values (" + variable.Supplier_ID + ",'" + variable.Name + "','" + variable.Tphone + "', '" + variable.Email + "', '" + variable.Adress + "'  )";
            SqlCommand name1 = new SqlCommand(query1, Sales2);
            Sales2.Open();
            name1.ExecuteNonQuery();
            Sales2.Close();

            //return View();
            return RedirectToAction("Supplier_Detail_view");

        }


        //Edit supplier details
        [Authorize]
        [HttpGet]
        public ActionResult Supplier_Detail_Edit(int sid)
        {

            SqlConnection Sales = new ExecuteSqlCommand().getConnection();

            string name2 = "SELECT  Supplier_ID,Name,Tphone,Email,Adress  FROM   Customer_Details WHERE Supplier_ID=" + sid;
            List<Customer_Details> LIST1 = new List<Customer_Details>();

            SqlCommand Sales1 = new SqlCommand(name2, Sales);
            Sales.Open();
            var read = Sales1.ExecuteReader();
            Customer_Details CD = new Customer_Details();
            while (read.Read())
            {

                CD.Supplier_ID = Convert.ToInt32(read["Supplier_ID"]);
                CD.Name = read["Name"].ToString();
                CD.Tphone = read["Tphone"].ToString();
                CD.Email = read["Email"].ToString();
                CD.Adress = read["Adress"].ToString();

            }
            Sales.Close();

            return View(CD);



        }

        [Authorize]
        [HttpPost]
        public ActionResult Supplier_Detail_Edit(Customer_Details variable)
        {

            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();
            string query3 = "Update Customer_Details set Supplier_ID=" + variable.Supplier_ID + ",Name ='" + variable.Name + "',Tphone='" + variable.Tphone + "', Email='" + variable.Email + "', Adress='" + variable.Adress + "'  ";
            SqlCommand name4 = new SqlCommand(query3, Sales4);
            Sales4.Open();
            name4.ExecuteNonQuery();
            Sales4.Close();

            return RedirectToAction("Supplier_Detail_view");

        }


        //Delete supplier details

        //[HttpGet]
        //public ActionResult supplier_Detail_Delete()
        //{
        //    return View();

        //}



        //[HttpPost]
        [Authorize]
        public ActionResult supplier_Detail_Delete(int sid)
        {

            SqlConnection Sales3 = new ExecuteSqlCommand().getConnection();
            string deleteQuery = "DELETE FROM Customer_Details WHERE Supplier_ID = " + sid;
            SqlCommand name5 = new SqlCommand(deleteQuery, Sales3);
            Sales3.Open();
            name5.ExecuteNonQuery();
            Sales3.Close();


            return View("LIST1");

        }
        //insert sales order detalis

        [Authorize]
        [HttpGet]

        public ActionResult Sales_Order_Inset()
        {
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sales_Order_Inset(Sales_Order variable1)
        {


            SqlConnection Sales3 = new ExecuteSqlCommand().getConnection();



            string query2 = "Insert into Sales_Order(Invoice_No,Supplier_id,Descriptions,Date,Quntity,Unit_Price) values (" + variable1.Invoice_No + "," + variable1.Supplier_id + ",'" + variable1.Descriptions + "', '" + variable1.Date + "', " + variable1.Quntity + "," + variable1.Unit_Price + "  )";
            SqlCommand name4 = new SqlCommand(query2, Sales3);
            Sales3.Open();
            name4.ExecuteNonQuery();
            Sales3.Close();


            return View();

        }

        //insert multiple data into the sales order

        [Authorize]
        public ActionResult Sales_Order_Insetmulti()
        {
            List<Sales_Order> so = new List<Sales_Order> { new Sales_Order { Invoice_No = 0, Supplier_id = 0, Descriptions = "", Date = "", Quntity = 0, Unit_Price = 0 } };
            return View(so);
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Sales_Order_Insetmulti(List<Sales_Order> so)
        {
            if (ModelState.IsValid)
            {

                SqlConnection Sales3 = new ExecuteSqlCommand().getConnection();

                Sales3.Open();
                foreach (var sos in so)
                {
                    string query2 = "Insert into Sales_Order(Invoice_No,Supplier_id,Descriptions,Date,Quntity,Unit_Price) values (" + sos.Invoice_No + "," + sos.Supplier_id + ",'" + sos.Descriptions + "', '" + sos.Date + "', " + sos.Quntity + "," + sos.Unit_Price + "  )";
                    SqlCommand name4 = new SqlCommand(query2, Sales3);

                    name4.ExecuteNonQuery();

                }
                Sales3.Close();

            }
            return View(so);

        }
        //public ActionResult Sales_Order_view()
        //{
        //    return View();
        //}
        ////View sales order  details

        //[HttpPost]
        [Authorize]
        public ActionResult Sales_Order_view()
        {


            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();

            string name3 = "SELECT Invoice_No ,Supplier_id , Descriptions,Date,Quntity,Unit_Price FROM   Sales_Order";
            List<Sales_Order> LIST2 = new List<Sales_Order>();

            SqlCommand Sales2 = new SqlCommand(name3, Sales4);
            Sales4.Open();
            var read = Sales2.ExecuteReader();

            while (read.Read())
            {


                Sales_Order SO = new Sales_Order();

                SO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SO.Descriptions = read["Descriptions"].ToString();
                SO.Date = read["Date"].ToString();
                SO.Quntity = Convert.ToInt32(read["Quntity"]);
                SO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);
                LIST2.Add(SO);




            }
            Sales4.Close();

            return View(LIST2);

        }



        //Edit sales order


        [Authorize]
        public ActionResult Sales_Order_Edit(int id)
        {


            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();

            string name3 = "SELECT Invoice_No ,Supplier_id , Descriptions,Date,Quntity,Unit_Price FROM   Sales_Order WHERE Invoice_No=" + id;
            List<Sales_Order> LIST2 = new List<Sales_Order>();

            SqlCommand Sales2 = new SqlCommand(name3, Sales4);
            Sales4.Open();
            var read = Sales2.ExecuteReader();
            Sales_Order SO = new Sales_Order();
            while (read.Read())
            {



                SO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SO.Descriptions = read["Descriptions"].ToString();
                SO.Date = read["Date"].ToString();
                SO.Quntity = Convert.ToInt32(read["Quntity"]);
                SO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);





            }
            Sales4.Close();

            return View(SO);


        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]

        public ActionResult Sales_Order_Edit(Sales_Order variable)
        {

            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();
            string query3 = "Update Sales_Order set Invoice_No =" + variable.Invoice_No + ",Supplier_id =" + variable.Supplier_id + ", Descriptions='" + variable.Descriptions + "', Date='" + variable.Date + "', Quntity=" + variable.Quntity + " ,Unit_Price=" + variable.Unit_Price + " ";
            SqlCommand name4 = new SqlCommand(query3, Sales4);
            Sales4.Open();
            name4.ExecuteNonQuery();
            ViewBag.Message = "Data Succesfully Inserted";
            Sales4.Close();

            return RedirectToAction("Sales_Order_view");

        }

        //Delete sales order 
        [Authorize]
        [HttpGet]
        public ActionResult sales_Order_Delete(int id)
        {

            SqlConnection Sales4 = new ExecuteSqlCommand().getConnection();

            string name3 = "SELECT Invoice_No ,Supplier_id , Descriptions,Date,Quntity,Unit_Price FROM   Sales_Order WhERE Invoice_No=" + id;
            List<Sales_Order> LIST3 = new List<Sales_Order>();

            SqlCommand Sales2 = new SqlCommand(name3, Sales4);
            Sales4.Open();
            var read = Sales2.ExecuteReader();
            Sales_Order SO = new Sales_Order();
            while (read.Read())
            {



                SO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SO.Descriptions = read["Descriptions"].ToString();
                SO.Date = read["Date"].ToString();
                SO.Quntity = Convert.ToInt32(read["Quntity"]);
                SO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);





            }
            Sales4.Close();

            return View(SO);






        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult sales_Order_Delete(Sales_Order variable2, int id)
        {

            SqlConnection Sales5 = new ExecuteSqlCommand().getConnection();
            string deleteQuery = "DELETE  FROM Sales_Order WHERE Invoice_No = " + id;
            SqlCommand name5 = new SqlCommand(deleteQuery, Sales5);
            Sales5.Open();
            name5.ExecuteNonQuery();
            ViewBag.Message = "Data Succesfully Deteted";
            Sales5.Close();


            return RedirectToAction("Sales_Order_view");

        }

        [HttpGet]
        public ActionResult GetGrnForPopUp(string grnNo)
        {
            string query = "spGetGrnFrom_GRN_NO '" + grnNo + "'";
            List<GRN> list = new BusinessLayer(query).GetGrn();

            return PartialView("GetGrnForPopUp_Partial", list);
        }

    }
}