﻿using IdentitySQLDemo.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PagedList.Mvc;
using PagedList;
using IdentitySQLDemo.DB_layer;

namespace IdentitySQLDemo.Controllers
{
    public class CustormerController : Controller
    {
        
        //
        // GET: /Custormer/
        [Authorize]
        public ActionResult Index(int? page)
        {

            string selectQuery = "SELECT ID,name,address,email,phone_no FROM customer";
            List<customer> customerList = new List<customer>();

            BusinessLayer businessLayer = new BusinessLayer(selectQuery);
            customerList = businessLayer.GetCustomer();

            return View(customerList.ToPagedList(page ?? 1, 6));

        }
        [Authorize]
        public ActionResult intsertCustomer()
        {
            return View();

        }

        [Authorize]
        [HttpPost]
        public ActionResult intsertCustomer(customer cus)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);
            string insertQuery = "INSERT INTO customer(ID,name,address,phone_no,email) VALUES (" + cus.ID + ",'" + cus.name + "','" + cus.address + "'," + cus.phone_no + ",'" + cus.email + "')";
            SqlCommand command2 = new SqlCommand(insertQuery, con);
            con.Open();
            command2.ExecuteNonQuery();
            con.Close();

            return RedirectToAction("intsertCustomer");

        }


        [Authorize]
        [HttpGet]
        public ActionResult search(string search, int? page)
        {
            List<customer> customerList = new List<customer>();
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);

            string searchQuery = "exec sp_search_customers '" + search + "'";
            
            SqlCommand command3 = new SqlCommand(searchQuery, con);
            con.Open();
            var read = command3.ExecuteReader();
            while (read.Read())
            {

                customer customers = new customer();

                customers.ID = Convert.ToInt32(read["ID"]);
                customers.name = read["name"].ToString();
                customers.address = read["address"].ToString();
                customers.phone_no = Convert.ToInt32(read["phone_no"]);
                customers.email = read["email"].ToString();


                customerList.Add(customers);




            }
            con.Close();

            return View(customerList.ToPagedList(page ?? 1, 6));

        }
        [Authorize]
        [HttpGet]
        public ActionResult editCustomer()
        {
            return View();

        }

        [Authorize]
        [HttpPost]
        public ActionResult editCustomer(customer cus, int id)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);
            string editQuery = "UPDATE customer SET ID =" + cus.ID + ",name ='" + cus.name + "',address ='" + cus.address + "',phone_no=" + cus.phone_no + ",email='" + cus.email + "'WHERE ID = " + id + "";
            SqlCommand command4 = new SqlCommand(editQuery, con);
            con.Open();
            command4.ExecuteNonQuery();
            con.Close();


            return RedirectToAction("Index");

        }
        [Authorize]
        [HttpGet]
        public ActionResult deleteCustomer()
        {
            return View();

        }

        [Authorize]
        [HttpPost]
        public ActionResult deleteCustomer(customer cus, int id)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);
            string deleteQuery = "DELETE FROM customer WHERE ID = " + id + "";
            SqlCommand command5 = new SqlCommand(deleteQuery, con);
            con.Open();
            command5.ExecuteNonQuery();
            con.Close();


            return RedirectToAction("customerList");

        }
        [Authorize]
        public ActionResult Index2(int id)
        {

            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);

            string selectQuery = "SELECT ID,item_name,description,unit_price,quantity FROM Purchase_Order WHERE ID = " + id + "";
            List<Purchase_Order> invoiceList = new List<Purchase_Order>();

            SqlCommand command1 = new SqlCommand(selectQuery, con);
            con.Open();
            var read = command1.ExecuteReader();
            while (read.Read())
            {

                Purchase_Order po = new Purchase_Order();

                po.ID = Convert.ToInt32(read["ID"]);
                po.item_name = read["item_name"].ToString();
                po.description = read["description"].ToString();
                po.unit_price = Convert.ToInt32(read["unit_price"]);
                po.quantity = Convert.ToInt32(read["quantity"]);

                invoiceList.Add(po);




            }
            con.Close();

            return View(invoiceList);

        }

        [Authorize]
        public PartialViewResult Index3(int id)
        {

            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con = new SqlConnection(connectionstring);

            string selectQuery = "SELECT ID,item_name,description,unit_price,quantity FROM Purchase_Order WHERE ID = " + id + "";
            List<Purchase_Order> invoiceList = new List<Purchase_Order>();

            SqlCommand command1 = new SqlCommand(selectQuery, con);
            con.Open();
            var read = command1.ExecuteReader();
            while (read.Read())
            {

                Purchase_Order po = new Purchase_Order();

                po.ID = Convert.ToInt32(read["ID"]);
                po.item_name = read["item_name"].ToString();
                po.description = read["description"].ToString();
                po.unit_price = Convert.ToInt32(read["unit_price"]);
                po.quantity = Convert.ToInt32(read["quantity"]);

                invoiceList.Add(po);




            }
            con.Close();

            return PartialView("_index3", invoiceList);

        }

        [Authorize]
        [HttpGet]
        public ActionResult insertInvoice()
        {

            List<Purchase_Order> po = new List<Purchase_Order> { new Purchase_Order { ID = 0, item_name = "", description = "", unit_price = 0, quantity = 0 } };
            return View(po);

        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult insertInvoice(List<Purchase_Order> po)
        {

            if (ModelState.IsValid)
            {
                string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
                SqlConnection con2 = new SqlConnection(connectionstring);

                con2.Open();
                foreach (var pos in po)
                {
                    string insertQuery = "INSERT INTO Purchase_Order(ID,item_name,description,unit_price,quantity) VALUES (" + pos.ID + ",'" + pos.item_name + "','" + pos.description + "'," + pos.unit_price + "," + pos.quantity + ")";
                    SqlCommand command2 = new SqlCommand(insertQuery, con2);

                    command2.ExecuteNonQuery();

                }
                ViewBag.Message = "Data succesfully saved";
                ModelState.Clear();
                con2.Close();

            }
            return View(po);
        }
        [Authorize]
        [HttpGet]
        public ActionResult editInvoice()
        {
            return View();
        }


        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult editInvoice(Purchase_Order po, int id)
        {
            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con3 = new SqlConnection(connectionstring);

            con3.Open();
            string editQuery = "UPDATE Purchase_Order SET ID=" + po.ID + ",item_name='" + po.item_name + "',description='" + po.description + "',unit_price=" + po.unit_price + ",quantity=" + po.quantity + " WHERE ID = " + id + "";
            SqlCommand command3 = new SqlCommand(editQuery, con3);

            command3.ExecuteNonQuery();


            ViewBag.Message = "Data succesfully saved";

            con3.Close();


            return View();
        }
        [Authorize]
        [HttpGet]
        public ActionResult deleteInvoice()
        {
            return View();
        }
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult deleteInvoice(Purchase_Order po, int id)
        {

            string connectionstring = ConfigurationManager.ConnectionStrings["UsersContext"].ConnectionString;
            SqlConnection con3 = new SqlConnection(connectionstring);

            con3.Open();
            string deleteQuery = "DELETE FROM Purchase_Order WHERE ID = " + id + "";
            SqlCommand command3 = new SqlCommand(deleteQuery, con3);

            command3.ExecuteNonQuery();
            con3.Close();
            return View();
        }
    }
}