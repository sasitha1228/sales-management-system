﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using AspNet.Identity.MySQL;
using Microsoft.Owin.Security;
using IdentitySQLDemo.Models;

namespace IdentitySQLDemo.Controllers
{
    public class RolesController : Controller
    {
        public RolesController()

            : this(new RoleManager<ApplicationRoles>(new RoleStore<ApplicationRoles>(new ApplicationDbContext("UsersContext"))))
        {
        }

        public RolesController(RoleManager<ApplicationRoles> roleManager)
        {
            RoleManager = roleManager;
        }

        public RoleManager<ApplicationRoles> RoleManager { get; private set; }
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult CreatRoles()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> CreatRoles(IdentityRole role)
        {

            if (ModelState.IsValid)
            {
                var rol = new ApplicationRoles() { Name = role.Name ,Id = role.Id };
                var result = await RoleManager.CreateAsync(rol);

                if (result.Succeeded)
                {
                    ViewBag.Massage = "Role Succesfully created";
                }
                else
                {
                    ViewBag.Massage = "Role Creation is not succesfull";
                }
            }
           

            return View();
        }

        /*[HttpGet]
        public ActionResult InsertUserToRoles()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InsertUserToRoles(InsertUsersToRolesModel model)
        {
            if (ModelState.IsValid)
            {
                

            }
            return View();
        }*/
	}
}