﻿$(function () {
    // Reference the auto-generated proxy for the hub.
    var chat = $.connection.chatHub;

    $('#NormalApprove-@item.NotificationId').click(function () {

        chat.server.updateNotification('@DateTime.Now', 'approved', $('#NormalApprove').val());

    });

    $('#NormalDisApprove-@item.NotificationId').click(function () {

        chat.server.updateNotification('@DateTime.Now', 'disapproved', $('#NormalDisApprove').val());

    });
});