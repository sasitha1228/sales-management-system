﻿using AspNet.Identity.MySQL;
using IdentitySQLDemo.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace IdentitySQLDemo.DB_layer
{
    public class BusinessLayer
    {
        private string Query;
        public BusinessLayer(string query)
        {
            Query = query;
        }

        public List<Notification> GetNotification()
        {
            SqlConnection con = new ExecuteSqlCommand().getConnection();
            SqlCommand cmd = new SqlCommand(Query, con);
            List<Notification> list = new List<Notification>();
            con.Open();
            var read = cmd.ExecuteReader();

            while (read.Read())
            {
                Notification notification = new Notification();

                notification.NotificationId = int.Parse(read["NotificationId"].ToString());
                notification.SenderId = read["SenderId"].ToString();
                notification.ReceiverId = read["ReceiverId"].ToString();
                notification.NotificationMsg = read["NotificationMsg"].ToString();
                notification.NotificationStatus = read["NotificationStatus"].ToString();
                notification.ReceiveTime = read["ReceiveTime"].ToString();
                notification.ApprovedTime = read["ApprovedTime"].ToString();
                notification.NotificationType = read["NotificationType"].ToString();

                list.Add(notification);

            }

            con.Close();
            return list;
        }

        public List<Privilage> GetPrivilageDb()
        {

            SqlConnection con = new ExecuteSqlCommand().getConnection();
            List<Privilage> list = new List<Privilage>();
            SqlCommand cmd = new SqlCommand(Query, con);
            con.Open();
            var read = cmd.ExecuteReader();
            while (read.Read())
            {
                Privilage privilage = new Privilage();

                privilage.UserId = read["UserId"].ToString();
                privilage.RoleId = read["RoleId"].ToString();
                privilage.Function_1 = read["Function_1"].ToString().Equals("1") ? true : false;
                privilage.Function_2 = read["Function_2"].ToString().Equals("1") ? true : false;
                privilage.Function_3 = read["Function_3"].ToString().Equals("1") ? true : false;

                list.Add(privilage);
            }
            return list;
        }

        public Company GetCompanyDetails()
        {
            SqlConnection con = new ExecuteSqlCommand().getConnection();

            SqlCommand cmd = new SqlCommand(Query, con);
            Company company = new Company();
            con.Open();
            var read = cmd.ExecuteReader();
            while (read.Read())
            {
                company.ComId = Convert.ToInt32(read["ComId"]);
                company.RegistrationNo = read["RegistrationNo"].ToString();
                company.CompanyName = read["CompanyName"].ToString();
                company.Email = read["Email"].ToString();
                company.PhoneNo = read["PhoneNo"].ToString();
                company.RegisteredCountry = read["RegisteredCountry"].ToString();
                company.StreetAddress = read["StreetAddress"].ToString();
            }

            return company;
        }

        public int InsertNotificaionsAndGetID(string receiver, string msg, string senderId, string type, DateTime date)
        {
            SqlConnection con = new ExecuteSqlCommand().getConnection();

            SqlCommand cmd = new SqlCommand(Query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add("@SenderUserName", SqlDbType.VarChar);
            cmd.Parameters.Add("@ReceiverName", SqlDbType.VarChar);
            cmd.Parameters.Add("@NotificationMsg", SqlDbType.NText);
            cmd.Parameters.Add("@ReceiveTime", SqlDbType.DateTime);
            cmd.Parameters.Add("@NotificationType", SqlDbType.VarChar);

            cmd.Parameters.Add("@NotiID", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.Parameters["@SenderUserName"].Value = senderId;
            cmd.Parameters["@ReceiverName"].Value = receiver;
            cmd.Parameters["@NotificationMsg"].Value = msg;
            cmd.Parameters["@ReceiveTime"].Value = date;
            cmd.Parameters["@NotificationType"].Value = type;

            con.Open();
            int result = cmd.ExecuteNonQuery();
            int notifiID = Convert.ToInt32(cmd.Parameters["@NotiID"].Value);
            con.Close();
            return notifiID;
        }

        public List<IdentityUser> GetUser()
        {
            SqlConnection con = new ExecuteSqlCommand().getConnection();

            List<IdentityUser> list = new List<IdentityUser>();
            SqlCommand cmd = new SqlCommand(Query, con);

            con.Open();
            var read = cmd.ExecuteReader();
            while (read.Read())
            {
                IdentityUser user = new IdentityUser();

                user.Id = read["Id"].ToString();
                user.UserName = read["UserName"].ToString();
                user.Name = read["Name"].ToString();
                user.Email = read["Email"].ToString();
                user.EmailConfirmed = read["EmailConfirmed"].ToString().Equals("1") ? true : false;
                user.PhoneNumber = read["PhoneNumber"].ToString();
                user.PhoneNumberConfirmed = read["PhoneNumberConfirmed"].ToString().Equals("1") ? true : false;
                user.LockoutEnabled = read["LockoutEnabled"].ToString().Equals("1") ? true : false;
                user.LockoutEndDateUtc = read["LockoutEndDateUtc"] as DateTime?;
                user.AccessFailedCount = int.Parse(read["AccessFailedCount"].ToString());

                list.Add(user);

            }
            con.Close();

            return list;
        }
        public List<GRN> GetGrn()
        {
            SqlConnection Sales2 = new ExecuteSqlCommand().getConnection();
            List<GRN> LIST1 = new List<GRN>();

            SqlCommand Sales3 = new SqlCommand(Query, Sales2);
            Sales2.Open();
            var read = Sales3.ExecuteReader();
            while (read.Read())
            {

                GRN grn = new GRN();

                grn.GRN_NO = read["GRN_NO"].ToString();
                grn.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                grn.Descriptions = read["Descriptions"].ToString();
                grn.Date = read["Date"].ToString();
                grn.Ordered_Quantity = Convert.ToInt32(read["Ordered_Quantity"]);
                grn.Delivered_Quantity = Convert.ToInt32(read["Delivered_Quantity"]);
                grn.Total_Value = Convert.ToInt32(read["Total_Value"]);
                grn.Discount = Convert.ToInt32(read["Discount"]);
                //grn.Net_Value = Convert.ToInt32(read["Net_Value"]);

                LIST1.Add(grn);

            }
            Sales2.Close();

            return LIST1;

        }

        public List<Sales_Return_Order> GetSalesReturnOrder()
        {
            List<Sales_Return_Order> list = new List<Sales_Return_Order>();
            SqlConnection con = new ExecuteSqlCommand().getConnection();
            SqlCommand cmd = new SqlCommand(Query, con);

            con.Open();
            var read = cmd.ExecuteReader();
            while (read.Read())
            {

                Sales_Return_Order SRO = new Sales_Return_Order();

                SRO.SalesRe_No = Convert.ToInt32(read["SalesRe_No"]);
                SRO.Invoice_No = Convert.ToInt32(read["Invoice_No"]);
                SRO.Supplier_id = Convert.ToInt32(read["Supplier_id"]);
                SRO.Descriptions = read["Descriptions"].ToString();
                SRO.Date = read["Date"].ToString();
                SRO.Quntity = Convert.ToInt32(read["Quntity"]);
                SRO.Unit_Price = Convert.ToInt32(read["Unit_Price"]);
                //SO.Total_price = Convert.ToInt32(read["Total_price"]);
                list.Add(SRO);




            }
            con.Close();

            return list;
        }

        public List<customer> GetCustomer()
        {
            List<customer> list = new List<customer>();
            SqlConnection con = new ExecuteSqlCommand().getConnection();
            SqlCommand command1 = new SqlCommand(Query, con);
            
            con.Open();
            var read = command1.ExecuteReader();
            while (read.Read())
            {
                customer customers = new customer();

                customers.ID = Convert.ToInt32(read["ID"]);
                customers.name = read["name"].ToString();
                customers.address = read["address"].ToString();
                customers.phone_no = Convert.ToInt32(read["phone_no"]);
                customers.email = read["email"].ToString();

                list.Add(customers);

            }
            con.Close();

            return list;
        }

        public int GetResultsForInvoiceCheckWithDb(string invoiceNo, string type)
        {
            string typeVar = "";
            if(String.Equals(type, "Sales_Order")){
                typeVar = "@Sales_Order_no";
            }
            else if (String.Equals(type, "GRN"))
            {
                typeVar = "@GRN_no";
            }

            SqlConnection con = new ExecuteSqlCommand().getConnection();
            
            SqlCommand cmd = new SqlCommand(Query, con);
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.Add(typeVar, SqlDbType.VarChar);

            cmd.Parameters.Add("@Value", SqlDbType.Int).Direction = ParameterDirection.Output;

            cmd.Parameters[typeVar].Value = invoiceNo;
            

            con.Open();
            int result = cmd.ExecuteNonQuery();
            int amount = Convert.ToInt32(cmd.Parameters["@Value"].Value);
            con.Close();

            return amount;
        }

    }
}