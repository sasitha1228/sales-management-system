﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using IdentitySQLDemo.Models;
using Microsoft.AspNet.SignalR.Hubs;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using AspNet.Identity.MySQL;
using IdentitySQLDemo.DB_layer;
using System.Web.Mvc;
using IdentitySQLDemo.Controllers;

namespace IdentitySQLDemo.Hubs

{
    public class ChatHub : Hub
    {
        
        //UserManager<ApplicationUser> userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("UsersContext")))
        //{
            
        //};

        public ChatHub()
            : this(new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext("UsersContext"))))
        {
        }

        public ChatHub(UserManager<ApplicationUser> userManager)
        {
            UserManager = userManager;
                        
        }
        
        public UserManager<ApplicationUser> UserManager { get; private set; }

        //Massage sending
        public void Send(string name, string message, string receiver)
        {
                Clients.User(receiver).addNewMessageToPage(name ,message);
                Clients.Caller.addNewMessageToPage(name, message);
                
                SaveToDb(receiver, message);
        }
        private readonly static ConnectionMapping<string> _connections =
            new ConnectionMapping<string>();

        //Send notification
        public void NotifiSender(string msg, string receiver, string type)
        {
            string query = "SpGetUserNameFromName '" + receiver + "'";

            string receiverUname = new ExecuteSqlCommand(query).ExecuteGetSingleValueCommand();

            string name = Context.User.Identity.Name;
            int notifiID = saveNotification(receiver, msg, name, type);
            //if (string.Equals(type, "urgent"))
            //{
            Clients.User(receiverUname).sendNotification(name, msg, type, notifiID);
            //}
            //else if (string.Equals(type, "normal"))
            //{
            //    Clients.User(receiver).addNotification(name, msg, type);
            //}
            
               
        }

        public void selfAuthanticationSend(string msg, string receiver, string type)
        {
            string query = "SpGetUserNameFromName '" + receiver + "'";

            string receiverUname = new ExecuteSqlCommand(query).ExecuteGetSingleValueCommand();

            string name = Context.User.Identity.Name;

            Clients.User(receiverUname).sendSelfNotification(name, msg, type);
        }
        public void normalNotification(string name, string msg)
        {
            
            Clients.Caller.addNotificationToPage();

        }

        //Check password
        public async Task<ActionResult> checkPassword(string pass, string name, string type)
        {
            var user = await UserManager.FindAsync(Context.User.Identity.Name, pass);
                         
            if (user != null)
            {
                Clients.Caller.sendError("true");
                replyNotification(true, name, type);
            }
            else
            {
                Clients.Caller.sendError("fasle");
                //replyNotification(false, name, type);
                //updateNotification(DateTime.Now, 'approved', )
            }
            return null;
            
        }

        public async Task<ActionResult> getListAuthantication(string pass, string id, string type) //helper function to initialize checkPassword for list authantication
        {
            var user = await UserManager.FindByIdAsync(id);

            await checkPassword(pass, user.UserName, type);

            return null;
        }
        public int saveNotification(string receiver, string msg, string senderId, string type)
        {
            string query = "spInsertIntoNotifications";
            BusinessLayer bsLayer = new BusinessLayer(query);
            int notifiID = bsLayer.InsertNotificaionsAndGetID(receiver, msg, senderId, type, DateTime.Now);

            return (notifiID);
            
        }

        public void updateNotification(string date, string status, string notifacationId)
        {
            string query = "spUpdateNotifications " + notifacationId + ",'" + date + "','" + status + "'";
            new ExecuteSqlCommand(query).ExecuteInsertCommad();
        }

        //Send reply to a notification
        public void replyNotification(bool val, string receiver, string type)
        {
            if (string.Equals(type, "PurchaseOrder"))
            {
                Clients.User(receiver).sendInvoiceReply(val.ToString(),type);
            }

            Clients.User(receiver).getNotficationReply(val.ToString());
        }

        //helper function for replyNotification to convert user id to user name
        public void listDisapprove(bool val, string receiverId, string type)
        {
            var user = UserManager.FindById(receiverId);
            replyNotification(val, user.UserName, type);
        }

        public void SendChatMessage(string who, string message)
        {
            string name = Context.User.Identity.Name;

            foreach (var connectionId in _connections.GetConnections(who))
            {
                Clients.Client(connectionId).addChatMessage(name + ": " + message);
            }
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;

            _connections.Add(name, Context.ConnectionId);

            return base.OnConnected();
        }

        //public override Task OnDisconnected(bool stopCalled)
        //{
        //    string name = Context.User.Identity.Name;

        //    _connections.Remove(name, Context.ConnectionId);

        //    return base.OnDisconnected(stopCalled);
        //}

        public override Task OnReconnected()
        {
            string name = Context.User.Identity.Name;

            if (!_connections.GetConnections(name).Contains(Context.ConnectionId))
            {
                _connections.Add(name, Context.ConnectionId);
            }

          
            return base.OnReconnected();
        }

        public void SaveToDb(string sender, string Msg)
        {
            string date = DateTime.Now.ToShortDateString();
            string time = DateTime.Now.TimeOfDay.ToString();
            string query = "spInsertIntoCoversations '" + Context.User.Identity.GetUserId() + "','" + Context.User.Identity.GetUserName() + "','" + sender + "', null,'" + Msg + "','" + time + "','" + date + "'";
            new ExecuteSqlCommand(query).ExecuteInsertCommad();
        }

        public void invoiceSender(string senderUserName, string msg, string receiverName, string type)
        {
            string query = "spGetUserNamrFromName '" + receiverName + "'";
            //string query2 = "spGetUserIdFromUserName '" + senderUserName + "'";

            //var senderId = new ExecuteSqlCommand(query2).ExecuteGetSingleValueCommand();
            var receiverUserName = new ExecuteSqlCommand(query).ExecuteGetSingleValueCommand();
            int nitifiId = saveNotification(receiverUserName, msg, Context.User.Identity.GetUserId(), type);
            Clients.User(receiverUserName).sendInvoice(senderUserName, msg, type);


        }

        public async Task<ActionResult> listAuthantication(string senderId, string type, string notificationId, string password)
        {
            var user = await UserManager.FindAsync(Context.User.Identity.Name, password);
            var receivingUser = await UserManager.FindByIdAsync(senderId);

            if (user != null)
            {
                Clients.Caller.sendErrorList("true");
                replyNotification(true, receivingUser.UserName, type);
            }
            else
            {
                Clients.Caller.sendErrorList("false");
            }

            return null;
        }

        public void GetAllConnectedUsers()
        {
            
        }

    }
}