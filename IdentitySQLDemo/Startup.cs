﻿using Microsoft.Owin;
using Owin;
[assembly: OwinStartupAttribute(typeof(IdentitySQLDemo.Startup))]
namespace IdentitySQLDemo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            app.MapSignalR();
        }
    }
}
